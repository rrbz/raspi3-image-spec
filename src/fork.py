#!/usr/bin/python3
# Copyright (c) 2019 Marco Marinello <mmarinello@sezf.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import argparse


parser = argparse.ArgumentParser(description='Create modified versions of base.yaml')

parser.add_argument("--template", default="base.yaml", help="Template to be extended")
parser.add_argument("forks", nargs="+", help="File[s] to be merged into base")

args = parser.parse_args()


template = None

if args.template == "base.yaml":
    BASEDIR = os.path.dirname(os.path.realpath(__file__))
    template = open(os.path.join(BASEDIR, "base.yaml"), "r")
else:
    template = open(args.template, "r")

dest = sys.stdout


for line in template.readlines():
    if "# EXTRA COMMANDS HERE\n" in line:
        indent = line.replace("# EXTRA COMMANDS HERE\n", "")
        for forkfile in args.forks:
            fork = open(forkfile)
            for fkline in fork.readlines():
                dest.write("%s%s" % (indent, fkline))
            fork.close()
    else:
        dest.write(line)


template.close()
