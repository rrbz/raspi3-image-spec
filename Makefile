LANG := $(if $(LANG),$(LANG),en_GB.UTF-8)

all: base edu gui edugui

clean:
	rm -rf build

yamls:
	mkdir -p build
	# base, just copy it
	cp src/base.yaml build/base.yaml
	# edu and edu gui
	python3 src/fork.py src/forks/edu.yaml > build/edu.yaml
	python3 src/fork.py src/forks/edu.yaml src/forks/gui.yaml > build/edugui.yaml
	# gui
	python3 src/fork.py src/forks/gui.yaml > build/gui.yaml

lang:
	mkdir -p build
	if ! grep $(LANG) src/locale.gen 2>&1 > /dev/null ; then \
		echo "Locale $(LANG) not in configured locales!"; \
		exit 1; \
	fi
	echo "LANG=$(LANG)" > build/locale
	cat src/keyboard.template > build/keyboard
	echo -n XKBLAYOUT=\" >> build/keyboard
	cat build/locale | sed -e 's/LANG=//' | cut -d '_' -f 1 | tr -d '\n' >> build/keyboard
	echo \" >> build/keyboard

base: lang yamls
	$(MAKE) buildyaml SLUG=base SRC=build/base.yaml

edu: lang yamls
	$(MAKE) buildyaml SLUG=edu SRC=build/edu.yaml

gui: lang yamls
	$(MAKE) buildyaml SLUG=gui SRC=build/gui.yaml

edugui: lang yamls
	$(MAKE) buildyaml SLUG=edugui SRC=build/edugui.yaml

buildyaml:
	@echo Building $(SLUG) from $(SRC)
	mkdir -p build
	sudo ./vmdb2/vmdb2 --rootfs-tarball=build/$(SLUG).tar.gz --output build/$(SLUG).img --log build/$(SLUG).log --verbose $(SRC)
	sudo ./PiShrink/pishrink.sh build/$(SLUG).img build/$(SLUG)_shrinked.img
