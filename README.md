# Raspberry Pi 3 image spec

This repository contains the files with which the image referenced at
https://wiki.debian.org/RaspberryPi3 has been built.

## Building your own image

To build a Debian buster Raspberry Pi 3 image you should first clone this repository:

```shell
git clone --recursive --depth=1 https://gitlab.com/rrbz/raspi3-image-spec.git
cd raspi3-image-spec
```

For this you will first need to install `vmdb2`. As of July 2018, this
repository still ships vmdb2, but will probably be deprecated in the
future. You can choose:

- `vmdb2` is available as a package for Debian Testing and
  Unstable. However, we require at least [one
  feature](http://git.liw.fi/vmdb2/commit/?id=474cd53826fda5a571cca8b5dda7cf949291dd62)
  that has not been included in any of the releases uploaded to
  Debian.

  Therefore, `vmdb2` is presented as a submodule in this
  project. First install the
  [requirements](http://git.liw.fi/vmdb2/tree/README) of `vmdb2`:

	```shell
	apt install kpartx parted qemu-utils qemu-user-static python3-cliapp python3-jinja2 python3-yaml python3-cliapp debootstrap
	```

  Note that `python3-cliapp` is not available in Stretch, but as it
  does not carry any dependencies, can be manually installed by
  [fetching its .deb package ](https://packages.debian.org/buster/python3-cliapp)
  and installing it manually.

  You can run

  ```shell
  ./configure
  ```

  to make sure all dependencies are installed.

In the src/base.yaml is specified which repository should be used (ftp.de.debian.org is set by default)

- The images now build correctly with the main repository. 
  The default configuration will build your image following the regular Testing (*buster*)
  distribution as far as stable (*stretch*) is not supported (we require linux ≥ 4.14
  and raspi3-firmware ≥ 1.20171201-1).

- Testing is, however, constantly changing. You might want to choose a
  specific point in its history to build with. To do this, locate the
  line with `qemu-debootstrap: buster` in `raspi3.yaml`. Change
  `mirror: http://deb.debian.org/debian` to a known-good point in
  time. One such point can be `mirror:
  https://snapshot.debian.org/archive/debian/20181204T164956Z/ `.
    - Due to a
      [missing feature](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=763419)
      on snapshots, to make the build work, you have to disable an
      expiration check by APT. To do so, edit raspi3.yaml to replace
      all `apt-get` invocations with `apt-get -o
      Acquire::Check-Valid-Until=false`

```shell
# To compile the very very basic image
make base

# To compile the modified version for schools
make edu
```

## Installing the image onto the Raspberry Pi 3

Plug an SD card which you would like to entirely overwrite into your SD card reader.

Assuming your SD card reader provides the device `/dev/sdb`
(**Beware** If you choose the wrong device, you might overwrite
important parts of your system.  Double check it's the correct
device!), copy the image onto the SD card:

```shell
sudo dd if=src/youshrinked.img of=/dev/sdX bs=64k oflag=dsync status=progress
```

Then, plug the SD card into the Raspberry Pi 3 and power it up.

The image uses the hostname `rpi3`, so assuming your local network
correctly resolves hostnames communicated via DHCP, you can log into
your Raspberry Pi 3 once it booted:

```shell
ssh pi@rpi3
# Enter password “raspberry”
```


## Creating your own fork

1. Fork this repository
2. Create your file \*_fork.yaml 
3. Write there your own vmdb2 instructions, starting from zero indentation
4. Adapt the Makefile as for edu
5. Enjoy
